// this.Documents = new Mongo.Collection('counter');

if (Meteor.isClient) {
  var img_data = [
    {
      img_src: 'face.jpg',
      img_alt: 'some face'
    },
    {
      img_src: 'psycho.jpg',
      img_alt: 'some psycho'
    }
  ];

  Template.img_gallery.helpers({images: img_data});
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup

  });
}
